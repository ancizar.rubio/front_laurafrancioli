import { environment } from './environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from './../models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) { }

  getCurrentUser() {
    return this.http.get<User[]>(`${environment.apiUrl}/auth/user/`);
  }

  updateCurrentUser(userCurrentData) {
    return this.http.put<User[]>(`${environment.apiUrl}/auth/user/`, userCurrentData);
  }

  getAll(idUser) {
    return this.http.get<User[]>(`${environment.apiUrl}/dastosusuario/${idUser}`);
  }

  userCreateData(userData) {
    return this.http.post(`${environment.apiUrl}/dastosusuario/post/`, userData);
  }

  userUpdate(emailUser, userData) {
    return this.http.put(`${environment.apiUrl}/dastosusuario/update/${emailUser}/`, userData);
  }

  getDepartamentos() {
    return this.http.get(`${environment.apiUrl}/dastosusuario/departamentos/`)
  }

  setAddrs(direccion) {
    return this.http.post(`${environment.apiUrl}/direcusuario/create/direccion/`, direccion)
  }

  getUserByCotizacion(email) {
    return this.http.get(`${environment.apiUrl}/dastosusuario/userdata/${email}/`)
  }

}
