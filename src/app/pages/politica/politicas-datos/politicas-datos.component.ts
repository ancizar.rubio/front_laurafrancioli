import { Component, OnInit, OnChanges } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Router } from '@angular/router';

@Component({
  selector: 'app-politicas-datos',
  templateUrl: './politicas-datos.component.html',
  styleUrls: ['./politicas-datos.component.scss']
})
export class PoliticasDatosComponent implements OnInit {
  public expand = true;
  public hideExp: boolean = true;

  constructor(public breakpointObserver: BreakpointObserver, private router: Router) { }

  ngOnInit() {
    this.breakpointObserver
      .observe(['(max-width: 1279px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.expand = false;
          this.hideExp = false;
        } else {
          this.hideExp = true;
          this.expand = true;
        }
      });
    let urlhash = this.router.url
    let newHash = urlhash.split("#", -1)[1]
    console.log(newHash)
    setTimeout(() => {
      this.scroll(document.getElementById(newHash))
    }, 800);

  }

  OnChanges() {
    let urlhash = this.router.url
    let newHash = urlhash.split("#", -1)[1]
    console.log(newHash)
    setTimeout(() => {
      this.scroll(document.getElementById(newHash))
    }, 800);

  }

  scroll(elem: HTMLElement) {
    elem.scrollIntoView({ behavior: 'smooth' });
  }

}
