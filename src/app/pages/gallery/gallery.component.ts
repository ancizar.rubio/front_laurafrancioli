import { GalleryService } from './../../services/gallery.service';
import { GalleryImage } from './../../models/gallery-image';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { filter, map } from 'rxjs/operators';
import { GalleryModalComponent } from '../gallery-modal/gallery-modal.component';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnDestroy {
  subscription: Subscription[] = [];
  columns = 14;
  gallery: GalleryImage[] = [];

  constructor( public dialog: MatDialog,
    public galleryService: GalleryService,
    public mediaObserver: MediaObserver
  ) { }

  openDialog(position: number): void {
    this.galleryService.selectImage(position);
    this.dialog.open(GalleryModalComponent, {panelClass: 'custom-dialog-container'});
  }

  ngOnInit(): void {
    this.galleryService.createGallery();
    this.mediaChange();
    this.getGallery();
  }

  getGallery(): void {
    this.subscription.push(
      this.galleryService.getGallery().subscribe(gallery => this.gallery = gallery)
    );
  }

  private mediaChange(): void {
    this.subscription.push(
      this.mediaObserver.asObservable()
        .pipe(
          filter((changes: MediaChange[]) => changes.length > 0),
          map((changes: MediaChange[]) => changes[0])
        ).subscribe((change: MediaChange) => {
        switch (change.mqAlias) {
          case 'xs': {
            this.columns = 1;
            break;
          }
          case 'sm': {
            this.columns = 2;
            break;
          }
          case 'md': {
            this.columns = 3;
            break;
          }
          case 'lg': {
            this.columns = 5;
            break;
          }
          default: {
            this.columns = 9;
            break;
          }
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.forEach(subscription => subscription.unsubscribe());
  }

}
