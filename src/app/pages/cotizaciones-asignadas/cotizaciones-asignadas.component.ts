import { ModalDetailUserCotizacionComponent } from './../components/modal-detail-user-cotizacion/modal-detail-user-cotizacion.component';
import { ModalEvidenciasComponent } from './../components/cotizaciones-asignadas/modal-evidencias/modal-evidencias.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from './../../authService/user.service';
import { CotizadorService } from './../../services/cotizador.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { environment } from 'src/app/authService/environment.prod';
import { EmailSendToClientComponent } from '../components/email-send-to-client/email-send-to-client.component';

@Component({
  selector: 'app-cotizaciones-asignadas',
  templateUrl: './cotizaciones-asignadas.component.html',
  styleUrls: ['./cotizaciones-asignadas.component.scss']
})
export class CotizacionesAsignadasComponent implements OnInit {

  env = environment.apiUrl;
  panelOpenState = false;
  disableEXpand = false;
  quequieres = [];
  paraque = [];
  dimensiones = [];
  cotizaciones = [];
  getImagesCotizacion = [];
  getImagesProduccionCotizacion = [];
  properties = [];
  formActionSelect: FormGroup;
  gestorComercial = false;
  design = false;
  coordinador = false;
  produccion = false;
  showImagesToDesign = false;
  showImagesToProduccion = false;
  userPk: any;
  getImagen1: File = null;
  getImagen2: File = null;
  getImagen3: File = null;
  getAllImages = []
  dataClient = []
  showDistribucion = false;

  @ViewChild('imagenUp1', { static: false }) imagenUp1: ElementRef;
  @ViewChild('imagenUp2', { static: false }) imagenUp2: ElementRef
  @ViewChild('imagenUp3', { static: false }) imagenUp3: ElementRef

  constructor(
    public dialog: MatDialog,
    public cotizacionesAll: CotizadorService,
    private _snackBar: MatSnackBar,
    public user: UserService,
    private fb: FormBuilder,
    private getUserByCotizacion: UserService,) { }
  /*
  getAllParaque() {
      return this.http.get(`${environment.apiUrl}/cotizador/opcionparaque/`);
    }

    getAllDimensiones() {
      return this.http.get(`${environment.apiUrl}/cotizador/opciondimensiones/`);
    }
  */
  ngOnInit() {
    this.formActionSelect = this.fb.group({
      actionSelf: ['', [Validators.required]],
    });
    this.getAllCotizaciones();
  }

  cotizacionLength = 0;

  getAllCotizaciones() {
    this.cotizaciones = [];
    this.user.getCurrentUser().subscribe(usuario => {
      console.log(usuario['pk'])
      this.userPk = usuario['pk'];
      this.cotizacionesAll.getAllQue().subscribe(queQuiere => {
        this.quequieres.push(queQuiere);
        console.log(this.quequieres)
        this.cotizacionesAll.getAllParaque().subscribe(paraQue => {
          this.paraque.push(paraQue);
          console.log(this.paraque)
          this.cotizacionesAll.getAllDimensiones().subscribe(dimensions => {
            this.dimensiones.push(dimensions)
            console.log(this.dimensiones);
            this.cotizacionesAll.getAllCotizaciones(usuario['pk']).subscribe(allCotizaciones => {
              this.user.getAll(usuario['pk']).subscribe(data => {
                console.log(data[0]['roles'])
                this.cotizacionesAll.getCotizacionesByFilter(data[0]['roles'] === 'Gestión Comercial' ? 'En Gestión' : data[0]['roles'] === 'Diseño' ? 'En Disenio' : data[0]['roles'] === 'Coordinador' ? 'En Revisión de arte' : data[0]['roles'] === 'Producción' ? 'En Producción' : data[0]['roles'] === 'Distribución' ? 'En Distribución' : '').subscribe(cotizacionByRol => {
                  console.log(cotizacionByRol)
                  this.cotizaciones.push(cotizacionByRol);
                  console.log(this.cotizaciones)
                  this.cotizaciones[0].forEach(itemCotizacion => {
                    let cotizacionFilter = new Array({ 'que_quieres': itemCotizacion.que_quieres, })
                  })
                })
              })
            })
          })
        })
      })
      this.user.getAll(usuario['pk']).subscribe(data => {
        console.log(data)
        if (data[0]['roles'] !== null) {
          data[0]['roles'] === 'Gestion Comercial' ? this.gestorComercial = true : this.gestorComercial = false;
          if (data[0]['roles'] === 'Gestion Comercial') {
            this.properties = ['Diseño', 'Coordinador', 'Producción', 'Distribución']
            this.design = false;
            this.coordinador = false;
            this.produccion = false;
            this.showImagesToDesign = true;
            this.showImagesToProduccion = true;
            this.showDistribucion = false
          }
          if (data[0]['roles'] === 'Diseño') {
            this.properties = ['Coordinador'];
            this.design = true;
            this.coordinador = false;
            this.produccion = true;
            this.showImagesToProduccion = false;
            this.showImagesToDesign = true;
            this.showDistribucion = false
          }
          if (data[0]['roles'] === 'Coordinador') {
            this.properties = ['Diseño', 'Producción'];
            this.design = true;
            this.coordinador = true;
            this.produccion = true;
            this.showImagesToProduccion = true;
            this.showImagesToDesign = true;
            this.showDistribucion = false
          }
          if (data[0]['roles'] === 'Producción') {
            this.properties = ['Distribución'];
            this.design = true;
            this.coordinador = false;
            this.produccion = true;
            this.showImagesToProduccion = true;
            this.showImagesToDesign = false;
            this.showDistribucion = false
          }
          if (data[0]['roles'] === 'Distribución') {
            this.design = false;
            this.coordinador = false;
            this.produccion = false;
            this.showImagesToProduccion = false;
            this.showImagesToDesign = false;
            this.showDistribucion = true
          }
        }
      })
    })
  }

  getImagenByContizacion(id) {
    console.log(id)
    this.getImagesCotizacion = []
    this.cotizacionesAll.getImagesByCotizacion(id).subscribe(images => {
      console.log(images);
      this.getImagesCotizacion.push(images);
      console.log(this.getImagesCotizacion);
    })
  }

  selfCotizacion(id) {

    const valueForm = this.formActionSelect.get('actionSelf').value === 'Diseño' ? 'En Disenio' : this.formActionSelect.get('actionSelf').value === 'Coordinador' ? 'En Revisión de arte' : this.formActionSelect.get('actionSelf').value === 'Producción' ? 'En Producción' : this.formActionSelect.get('actionSelf').value === 'Distribución' ? 'En Distribución' : '';
    console.log(valueForm);
    const dataUpdate = { 'estado_cotizacion': valueForm }
    this.cotizacionesAll.updateCotizacion(id, dataUpdate).subscribe(res => {
      console.log(res);
      window.scrollTo(0, 0);
      this.getAllCotizaciones();
      this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
    }, err => {
      console.log(err)
      this._snackBar.open('Error al actualizar la cotizacion, por favor intente más tarde', 'ok')
    })
  }

  fileProgress(fileInput: any) {
    // this.getImagen1 = <File>fileInput.target.files[0];
    if (this.getImagen1 == undefined) {
      this.getImagen1 = <File>fileInput.target.files[0];
    } else if (this.getImagen2 == undefined) {
      this.getImagen2 = <File>fileInput.target.files[0];
    } else if (this.getImagen3 == undefined) {
      this.getImagen3 = <File>fileInput.target.files[0];
    }

    console.log(this.getImagen1, this.getImagen2, this.getImagen3)
    this.getAllImages.push({ 'nombre': 'imagen4', 'img': this.getImagen1 }, { 'nombre': 'imagen5', 'img': this.getImagen2 }, { 'nombre': 'imagen6', 'img': this.getImagen3 })
  }

  openModalEvidencias(idCotizacion) {
    const dialogRef = this.dialog.open(ModalEvidenciasComponent, {
      panelClass: 'modalCotizacionesAsignadas',
      data: { 'idCotizacion': idCotizacion, 'userPk': this.userPk, 'properties': this.properties, 'rol': this.coordinador === true ? 'Coordinador' : this.showDistribucion === true ? 'distribucion' : this.showImagesToDesign === true ? 'design' : 'produccion' },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      window.scrollTo(0, 0);
      this.getAllCotizaciones();
      // this.animal = result;
    });
  }

  showInfoClient(email) {
    this.getUserByCotizacion.getUserByCotizacion(email).subscribe(resData => {
      console.log(resData);
      this.dataClient = resData[0];

      const dialogRef = this.dialog.open(ModalDetailUserCotizacionComponent, {
        panelClass: 'modalCotizacionesAsignadas',
        data: this.dataClient
      });

      // dialogRef.afterClosed().subscribe(result => {
      //   console.log('The dialog was closed');
      //   window.scrollTo(0, 0);
      //   this.getAllCotizaciones();
      //   // this.animal = result;
      // });
    })
  }

  sendEmailToclient(user, cotizacion, imagenes) {
    console.log(user);
    console.log(cotizacion);
    console.log(imagenes);
    const dialogRef = this.dialog.open(EmailSendToClientComponent, {
      panelClass: 'modalCotizacionesAsignadas',
      data: [user, cotizacion, imagenes]
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'ok') {
        this._snackBar.open('Email enviado con éxito.', 'ok')
      } else {
        this._snackBar.open('Error al enviar el email, intente más tarde.', 'ok')
      }

    });

  }

}
