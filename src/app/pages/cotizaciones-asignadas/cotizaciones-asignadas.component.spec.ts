import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizacionesAsignadasComponent } from './cotizaciones-asignadas.component';

describe('CotizacionesAsignadasComponent', () => {
  let component: CotizacionesAsignadasComponent;
  let fixture: ComponentFixture<CotizacionesAsignadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizacionesAsignadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizacionesAsignadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
