import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from './../../authService/user.service';
import { CotizadorService } from './../../services/cotizador.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/authService/environment.prod';

@Component({
  selector: 'app-cotizaciones',
  templateUrl: './cotizaciones.component.html',
  styleUrls: ['./cotizaciones.component.scss']
})
export class CotizacionesComponent implements OnInit {

  env = environment.apiUrl;
  panelOpenState = false;
  disableEXpand = false;
  quequieres = [];
  paraque = [];
  dimensiones = [];
  cotizaciones = [];
  getImagesCotizacion = [];
  properties = [];
  formActionSelect: FormGroup;
  constructor(public cotizacionesAll: CotizadorService, public user: UserService, private fb: FormBuilder) { }
  /*
  getAllParaque() {
      return this.http.get(`${environment.apiUrl}/cotizador/opcionparaque/`);
    }

    getAllDimensiones() {
      return this.http.get(`${environment.apiUrl}/cotizador/opciondimensiones/`);
    }
  */
  ngOnInit() {
    this.formActionSelect = this.fb.group({
      actionSelf: ['', [Validators.required]]
    });
    this.user.getCurrentUser().subscribe(usuario => {
      console.log(usuario['pk'])
      this.cotizacionesAll.getAllQue().subscribe(queQuiere => {
        this.quequieres.push(queQuiere);
        console.log(this.quequieres)
        this.cotizacionesAll.getAllParaque().subscribe(paraQue => {
          this.paraque.push(paraQue);
          console.log(this.paraque)
          this.cotizacionesAll.getAllDimensiones().subscribe(dimensions => {
            this.dimensiones.push(dimensions)
            console.log(this.dimensiones);
            this.cotizacionesAll.getAllCotizaciones(usuario['pk']).subscribe(allCotizaciones => {
              this.cotizaciones.push(allCotizaciones);
              console.log(this.cotizaciones)
              this.cotizaciones[0].forEach(itemCotizacion => {
                let cotizacionFilter = new Array({ 'que_quieres': itemCotizacion.que_quieres, })
              })

            })
          })
        })
      })

    })
  }

  getImagenByContizacion(id) {
    console.log(id)
    this.getImagesCotizacion = []
    this.cotizacionesAll.getImagesByCotizacion(id).subscribe(images => {
      console.log(images);
      this.getImagesCotizacion.push(images);
      console.log(this.getImagesCotizacion);
    })
  }

}
