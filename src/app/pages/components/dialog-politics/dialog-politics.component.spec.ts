import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPoliticsComponent } from './dialog-politics.component';

describe('DialogPoliticsComponent', () => {
  let component: DialogPoliticsComponent;
  let fixture: ComponentFixture<DialogPoliticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPoliticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPoliticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
