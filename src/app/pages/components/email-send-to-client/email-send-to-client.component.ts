import { ContactoService } from './../../../services/contacto.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { environment } from 'src/app/authService/environment.prod';

@Component({
  selector: 'app-email-send-to-client',
  templateUrl: './email-send-to-client.component.html',
  styleUrls: ['./email-send-to-client.component.scss']
})
export class EmailSendToClientComponent implements OnInit {
  entorno = environment.apiUrl
  contactClient: FormGroup
  images: string;
  pendingSelf = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<EmailSendToClientComponent>, private fb: FormBuilder, private contact: ContactoService) { }

  ngOnInit() {
    console.log(this.data)

    const images = [
      this.data[2][0].imagen7,
      this.data[2][0].imagen8,
      this.data[2][0].imagen9
    ]

    console.log(images)
    const imagesFilter = images.filter(x => x !== null)
    console.log(imagesFilter)
    const links = [];
    imagesFilter.forEach(item => {
      const newLink = this.entorno + item;
      console.log(newLink);
      links.push(newLink)
    })

    // console.log(links.toString())
    const imagetoStr = links.toString().replace(',', '\n');
    this.images = imagetoStr;
    console.log(this.images);
    // const filterImage = links[0].filter(x => x.imagen !== '');
    // console.log(filterImage);

    this.contactClient = this.fb.group({
      commits: ['', [Validators.required]]
    })
  }

  sendMail() {
    this.pendingSelf = true;
    // this.contactClient.get('commits').patchValue(this.images);
    const msg = this.contactClient.get('commits').value;
    const resultMsg = msg + '\n' + 'Copia y pega cada link, para ver los diseños:' + '\n' + this.images;
    console.log(resultMsg);
    const dataToClient = {
      "name": 'Equipo Laura francioli',
      "message": resultMsg,
      "email": this.data[0]
    }
    this.contact.sendMailClient(dataToClient).subscribe(res => {
      this.dialogRef.close('ok');
      this.pendingSelf = false;
    }, err => {
      console.log(err)
      this.dialogRef.close('err');
      this.pendingSelf = false;
    })
  }

}
