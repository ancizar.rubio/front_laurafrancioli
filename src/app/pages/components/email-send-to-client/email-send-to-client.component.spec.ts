import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSendToClientComponent } from './email-send-to-client.component';

describe('EmailSendToClientComponent', () => {
  let component: EmailSendToClientComponent;
  let fixture: ComponentFixture<EmailSendToClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailSendToClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSendToClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
