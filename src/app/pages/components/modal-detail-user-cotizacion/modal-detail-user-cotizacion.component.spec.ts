import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetailUserCotizacionComponent } from './modal-detail-user-cotizacion.component';

describe('ModalDetailUserCotizacionComponent', () => {
  let component: ModalDetailUserCotizacionComponent;
  let fixture: ComponentFixture<ModalDetailUserCotizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetailUserCotizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetailUserCotizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
