import { CotizadorService } from './../../../services/cotizador.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-modal-detail-user-cotizacion',
  templateUrl: './modal-detail-user-cotizacion.component.html',
  styleUrls: ['./modal-detail-user-cotizacion.component.scss']
})
export class ModalDetailUserCotizacionComponent implements OnInit {

  arrUser = {}
  constructor(@Inject(MAT_DIALOG_DATA) public data, private getAllDirecusuario: CotizadorService) { }

  ngOnInit() {
    console.log(this.data)

    this.getAllDirecusuario.getAllDirecusuario(this.data.id).subscribe(address => {
      // console.log(address.length)

      console.log(address);

      const filterAdress = address.filter(x => x.id === this.data.Shipping_Address)
      console.log(filterAdress);

      this.arrUser = {
        Shipping_Address: filterAdress[0]['direccion'],
        ciudad: this.data.ciudad,
        // created: "2021-08-06T22:57:32.051641-05:00",
        // departamento: 147,
        // id: 7,
        identification: this.data.identification,
        persona: this.data.persona,
        // roles: "Gestion Comercial",
        // updated: "2021-08-12T22:07:58.799603-05:00",
        usuario: this.data.usuario,
        whatsapp: this.data.whatsapp,
      }
      console.log(this.arrUser)
      // this.allAddres = address;
    }, err => {
      console.log(err)
    })
  }

}
