import { UserService } from './../../../authService/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-modal-direcciones',
  templateUrl: './modal-direcciones.component.html',
  styleUrls: ['./modal-direcciones.component.scss']
})
export class ModalDireccionesComponent implements OnInit {

  addrsUserData: FormGroup;
  departamentos = [];
  AddrsRegister = [];

  constructor(public dialogRef: MatDialogRef<ModalDireccionesComponent>,
    @Inject(MAT_DIALOG_DATA) public data, public fb: FormBuilder, private userData: UserService) { }

  ngOnInit() {
    this.addrsUserData = this.fb.group({
      departamento: ['', Validators.required],
      ciudad: ['', Validators.required],
      direccion: ['', Validators.required]
    })
    this.userData.getDepartamentos().subscribe(dptos => {
      let dprts = [];
      let getDimension = this.data[1][0];
      console.log(getDimension, getDimension.dimension)
      console.log(dptos)
      dprts.push(dptos);
      console.log(dprts[0])
      let getFilterDptos = dprts[0].filter(x => x.dimesion.includes(getDimension.dimension))
      console.log(getFilterDptos)
      this.departamentos = getFilterDptos;

    })
  }

  saveAddrModal() {
    this.AddrsRegister.push(this.addrsUserData.value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
