import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEvidenciasComponent } from './modal-evidencias.component';

describe('ModalEvidenciasComponent', () => {
  let component: ModalEvidenciasComponent;
  let fixture: ComponentFixture<ModalEvidenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEvidenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEvidenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
