import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CotizadorService } from './../../../../services/cotizador.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';

@Component({
  selector: 'app-modal-evidencias',
  templateUrl: './modal-evidencias.component.html',
  styleUrls: ['./modal-evidencias.component.scss']
})
export class ModalEvidenciasComponent implements OnInit {

  getImagen4: File = null;
  getImagen5: File = null;
  getImagen6: File = null;
  getImagen7: File = null;
  getImagen8: File = null;
  getImagen9: File = null;
  getAllImages = [];
  getAllImagesTwo = [];
  formActionSelect: FormGroup;
  formActionSelectTwo: FormGroup;
  formActionSelectThree: FormGroup;
  @ViewChild('imagenUp4', { static: false }) imagenUp4: ElementRef;
  @ViewChild('imagenUp5', { static: false }) imagenUp5: ElementRef
  @ViewChild('imagenUp6', { static: false }) imagenUp6: ElementRef
  @ViewChild('imagenUp7', { static: false }) imagenUp7: ElementRef;
  @ViewChild('imagenUp8', { static: false }) imagenUp8: ElementRef
  @ViewChild('imagenUp9', { static: false }) imagenUp9: ElementRef
  properties = [];

  constructor(public _snackBar: MatSnackBar,
    public fb: FormBuilder,
    public cotizacionesAll: CotizadorService,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
    console.log(this.data);
    this.formActionSelect = this.fb.group({
      actionSelf: ['', [Validators.required]],
      imagen4: [''],
      imagen5: [''],
      imagen6: [''],
      comentarios: ['']
    });

    this.formActionSelectTwo = this.fb.group({
      actionSelf: ['', [Validators.required]],
      imagen7: [''],
      imagen8: [''],
      imagen9: [''],
      comentarios: ['']
    });
    this.formActionSelectThree = this.fb.group({
      actionSelf: ['', [Validators.required]],
      comentarios: ['']
    });

    // onValueChanges() {
    //   // console.log(this.isValidCotizacion);
    //   // console.log(this.formActionSelect.controls.imagen1.value !== '', this.formActionSelect.controls.imagen2.value !== '', this.formActionSelect.controls.imagen3.value !== '')
    //   if (this.formActionSelect.controls.imagen1.value !== '' || this.formActionSelect.controls.imagen2.value !== '' || this.formActionSelect.controls.imagen3.value !== '') {
    //     this.formActionSelect.valid;
    //   } else {
    //     this._snackBar.open('Seleccione al menos una imágen', 'OK', {
    //       duration: 4000,
    //     });
    //     this.formActionSelect.invalid;
    //   }

    // }
  }

  selfCotizacion(id) {

    const valueForm = this.formActionSelect.get('actionSelf').value === 'Diseño' ? 'En Disenio' : this.formActionSelect.get('actionSelf').value === 'Coordinador' ? 'En Revisión de arte' : this.formActionSelect.get('actionSelf').value === 'Producción' ? 'En Producción' : this.formActionSelect.get('actionSelf').value === 'Distribución' ? 'En Distribución' : '';
    const dataUpdate = { 'usuario': this.data.userPk, 'estado_cotizacion': valueForm, 'descripcion': this.formActionSelect.get('comentarios').value !== null ? this.formActionSelect.get('comentarios').value : null }

    const formData = new FormData()

    formData.append('cotizador', JSON.stringify(id))

    this.getAllImages.forEach(image => {
      console.log(image, image.img)
      if (image.img !== null) {
        formData.append(image.nombre, image.img)
      }
    })
    console.log(formData)
    this.cotizacionesAll.setImagesByCotizacion(formData).subscribe(imagesByCotRes => {
      this._snackBar.open('Imágenes cargadas correctamente.', 'ok')
      this.cotizacionesAll.updateCotizacion(id, dataUpdate).subscribe(res => {
        this._snackBar.dismiss();
        console.log(res);
        window.scrollTo(0, 0);
        this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
        // this.cotizacionesAll.setImagesByCotizacion(formData).subscribe(imagesByCotRes => {
        //   this._snackBar.open('Imágenes cargadas correctamente.', 'ok')
        // });
        // this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
      }, err => {
        console.log(err)
        this._snackBar.open('Error al actualizar la cotizacion, por favor intente más tarde', 'ok')
      })
    }, err => {
      this._snackBar.open('Error al cargar las imágenes.', 'ok')
    });
  }


  selfCotizacionTwo(id) {

    const valueForm = this.formActionSelectTwo.get('actionSelf').value === 'Diseño' ? 'En Disenio' : this.formActionSelectTwo.get('actionSelf').value === 'Producción' ? 'En Producción' : this.formActionSelectTwo.get('actionSelf').value === 'Distribución' ? 'En Distribución' : '';
    const dataUpdate = { 'usuario': this.data.userPk, 'estado_cotizacion': valueForm, 'descripcion': this.formActionSelectTwo.get('comentarios').value !== null ? this.formActionSelectTwo.get('comentarios').value : null }

    const formData = new FormData()

    formData.append('cotizador', JSON.stringify(id))

    this.getAllImagesTwo.forEach(image => {
      console.log(image, image.img)
      if (image.img !== null) {
        formData.append(image.nombre, image.img)
      }
    })
    console.log(formData)
    this.cotizacionesAll.setImagesByCotizacion(formData).subscribe(imagesByCotRes => {
      this._snackBar.open('Imágenes cargadas correctamente.', 'ok')
      this.cotizacionesAll.updateCotizacion(id, dataUpdate).subscribe(res => {
        this._snackBar.dismiss();
        console.log(res);
        window.scrollTo(0, 0);
        this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
        // this.cotizacionesAll.setImagesByCotizacion(formData).subscribe(imagesByCotRes => {
        //   this._snackBar.open('Imágenes cargadas correctamente.', 'ok')
        // });
        // this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
      }, err => {
        console.log(err)
        this._snackBar.open('Error al actualizar la cotizacion, por favor intente más tarde', 'ok')
      })
    }, err => {
      this._snackBar.open('Error al cargar las imágenes.', 'ok')
    });
  }

  fileProgress(fileInput: any) {
    // this.getImagen4 = <File>fileInput.target.files[0];
    if (this.getImagen4 == undefined) {
      this.getImagen4 = <File>fileInput.target.files[0];
      // console.log(this.getImagen4, this.getImagen5, this.getImagen6)
      this.getAllImages.push({ 'nombre': 'imagen4', 'img': this.getImagen4 })
      console.log(this.getAllImages)
    } else if (this.getImagen5 == undefined) {
      this.getImagen5 = <File>fileInput.target.files[0];
      // console.log(this.getImagen4, this.getImagen5, this.getImagen6)
      this.getAllImages.push({ 'nombre': 'imagen5', 'img': this.getImagen5 })
      console.log(this.getAllImages)
    } else if (this.getImagen6 == undefined) {
      this.getImagen6 = <File>fileInput.target.files[0]; console.log(this.getImagen4, this.getImagen5, this.getImagen6)
      this.getAllImages.push({ 'nombre': 'imagen6', 'img': this.getImagen6 })
      console.log(this.getAllImages)
    }

    // console.log(this.getImagen4, this.getImagen5, this.getImagen6)
    // this.getAllImages.push({ 'nombre': 'imagen4', 'img': this.getImagen4 }, { 'nombre': 'imagen5', 'img': this.getImagen5 }, { 'nombre': 'imagen6', 'img': this.getImagen6 })
    // console.log(this.getAllImages)
  }

  fileProgressTwo(fileInput: any) {
    // this.getImagen4 = <File>fileInput.target.files[0];
    if (this.getImagen7 == undefined) {
      this.getImagen7 = <File>fileInput.target.files[0];
      // console.log(this.getImagen7, this.getImagen5, this.getImagen9)
      this.getAllImagesTwo.push({ 'nombre': 'imagen7', 'img': this.getImagen7 })
      console.log(this.getAllImagesTwo)
    } else if (this.getImagen8 == undefined) {
      this.getImagen8 = <File>fileInput.target.files[0];
      // console.log(this.getImagen7, this.getImagen8, this.getImagen9)
      this.getAllImagesTwo.push({ 'nombre': 'imagen8', 'img': this.getImagen8 })
      console.log(this.getAllImagesTwo)
    } else if (this.getImagen9 == undefined) {
      this.getImagen9 = <File>fileInput.target.files[0]; console.log(this.getImagen7, this.getImagen8, this.getImagen9)
      this.getAllImagesTwo.push({ 'nombre': 'imagen9', 'img': this.getImagen9 })
      console.log(this.getAllImagesTwo)
    }

    // console.log(this.getImagen7, this.getImagen8, this.getImagen9)
    // this.getAllImages.push({ 'nombre': 'imagen7', 'img': this.getImagen7 }, { 'nombre': 'imagen8', 'img': this.getImagen8 }, { 'nombre': 'imagen9', 'img': this.getImagen6 })
    // console.log(this.getAllImages)
  }

  selfCotizacionThree(id) {

    const valueForm = this.formActionSelectThree.get('actionSelf').value === 'Diseño' ? 'En Disenio' : this.formActionSelectThree.get('actionSelf').value === 'Coordinador' ? 'En Revisión de arte' : this.formActionSelectThree.get('actionSelf').value === 'Producción' ? 'En Producción' : this.formActionSelectThree.get('actionSelf').value === 'Distribución' ? 'En Distribución' : '';
    const dataUpdate = { 'usuario': this.data.userPk, 'estado_cotizacion': valueForm, 'descripcion': this.formActionSelectThree.get('comentarios').value !== null ? this.formActionSelectThree.get('comentarios').value : null }
    console.log(valueForm)
    // const formData = new FormData()

    // formData.append('cotizador', JSON.stringify(id))

    // this.getAllImagesTwo.forEach(image => {
    //   console.log(image, image.img)
    //   if (image.img !== null) {
    //     formData.append(image.nombre, image.img)
    //   }
    // })
    // console.log(formData)

    this.cotizacionesAll.updateCotizacion(id, dataUpdate).subscribe(res => {
      this._snackBar.dismiss();
      console.log(res);
      window.scrollTo(0, 0);
      this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
      // this.cotizacionesAll.setImagesByCotizacion(formData).subscribe(imagesByCotRes => {
      //   this._snackBar.open('Imágenes cargadas correctamente.', 'ok')
      // });
      // this._snackBar.open('Muy bien, Cotización actualizada.', 'ok')
    }, err => {
      console.log(err)
      this._snackBar.open('Error al actualizar la cotizacion, por favor intente más tarde', 'ok')
    })
  }


}
