import { MatDialog } from '@angular/material/dialog';
import { ModalDireccionesComponent } from './../modal-direcciones/modal-direcciones.component';
import { CotizadorService } from './../../../services/cotizador.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from '../../../authService/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.scss']
})
export class UserformComponent implements OnInit {

  userForm: FormGroup;

  dataForm = [];

  enabledForm = false;

  enviando = true;

  page: any;
  paramsSub: any;
  userName = [];
  allAddres = []
  getUserData = []

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private getAllDirecusuario: CotizadorService,
    private dialog: MatDialog,
    private cotizador: CotizadorService
  ) { }

  ngOnInit() {

    this.paramsSub = this.activatedRoute.params.subscribe(
      params => (this.page = params['page']));

    this.userForm = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      identificacion: ['', [Validators.required]],
      whatsapp: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      Shipping_Address: ['', [Validators.required]],
    });

    this.disabledEnabled();

    let emailUser = localStorage.getItem('email');
    emailUser = JSON.parse(emailUser);

    this.userService.getCurrentUser().subscribe(getCurrentUsr => {
      if (getCurrentUsr !== undefined) {
        this.userName.push({ 'user': getCurrentUsr['username'], 'idUser': getCurrentUsr['pk'] });


        this.userForm.get('email').setValue(getCurrentUsr['email']);
        this.userForm.get('first_name').setValue(getCurrentUsr['first_name']);
        this.userForm.get('last_name').setValue(getCurrentUsr['last_name']);
        /*this.userForm.get('identificacion').setValue(data['identificacion']);
        this.userForm.get('whatsapp').setValue(data['whatsapp']);
        this.userForm.get('Shipping_Address').setValue(data['Shipping_Address']);*/

        this.userService.getAll(getCurrentUsr['pk']).subscribe(data => {
          console.log(data)
          this.getUserData = data;
          let getDataUser = data;
          if (data.length > 0) {

            this.userForm.get('identificacion').setValue(data[0].identification);
            this.userForm.get('whatsapp').setValue(data[0].whatsapp);
            console.log(data[0].Shipping_Address)
            if (data[0].Shipping_Address !== null) {
              this.getAllDirecusuario.getAllDirecusuario(getCurrentUsr['pk']).subscribe(address => {
                console.log(address.length)

                console.log(address);
                this.allAddres = address;
                this.userForm.get('Shipping_Address').setValue(getDataUser[0].Shipping_Address)

              }, err => {
                console.log(err)
              })

            } else {
              this.getAllDirecusuario.getAllDirecusuario(getCurrentUsr['pk']).subscribe(address => {
                console.log(address.length)
                if (address.length !== 0) {
                  console.log(address);
                  this.allAddres = address;
                  this.userForm.get('Shipping_Address').setValue(getDataUser[0].Shipping_Address)
                } else {
                  console.log('debe crear direcciones')
                }
              }, err => {
                console.log(err)
              })
            }
          }
        })

      }
    })

    this.cotizador.getAllDimensiones().subscribe(dim => {
      this.dimension.push(dim)
      console.log(dim)
    });

  }

  AddrsRegister = []
  dimension = []

  openModalAddrs(): void {
    // this.cotizadorForm.controls.dimensions.value, this.dimension

    let getDimensionSelected = this.dimension[0].filter(x => x.id == 2)
    const dialogRef = this.dialog.open(ModalDireccionesComponent, {
      width: '300px',
      data: [this.userName[0].idUser, getDimensionSelected]
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result)
      if (result !== undefined) {
        this.AddrsRegister = result;
        console.log(this.userName[0].idUser)
        this.userService.getAll(this.userName[0].idUser).subscribe(datosUser => {
          console.log(datosUser)
          let setDireccion = [
            {
              'usuario': this.userName[0].idUser,
              'direccion': this.AddrsRegister[0].direccion
            }
          ];
          this.userService.setAddrs(setDireccion).subscribe(resDireccion => {
            let updateDataUser =
            {
              "id": datosUser[0]['id'],
              "usuario": this.userName[0].idUser,
              "persona": datosUser[0]['persona'],
              "departamento": this.AddrsRegister[0].departamento,
              "ciudad": this.AddrsRegister[0].ciudad,
              "whatsapp": null,
              "identification": datosUser[0]['identification'],
              "Shipping_Address": resDireccion[0].id
            }
            console.log(updateDataUser);
            this.userService.userUpdate(datosUser[0]['id'], updateDataUser).subscribe(dataUser => {
              console.log(dataUser);
              this.getAllDirecusuario.getAllDirecusuario(datosUser[0]['id']).subscribe(address => {

                if (address.length !== 0) {
                  console.log(address);
                  this.allAddres = address;
                } else {
                  console.log('debe crear direcciones')
                }
              }, err => {
                console.log(err)
              })
              this._snackBar.open('Dirección agregada con éxito', 'ok', {
                duration: 4000,
              });

              this.userService.getAll(this.userName[0].idUser).subscribe(data => {
                console.log(data)

              })
            }, err => {
              console.log(err);
              this._snackBar.open('Error al agregar la dirección', 'ok', {
                duration: 4000,
              });
            })
          }, err => {
            console.log(err);
            this._snackBar.open('No se pudo guardar la dirección', 'ok', {
              duration: 4000,
            });
          })
        })
      } else {
        this._snackBar.open('Dirección cancelada', 'ok', {
          duration: 4000,
        });
      }
    });
  }

  public errorHandling = (control: string, error: string) => {
    return this.userForm.controls[control].hasError(error);
  }

  disabledEnabled() {
    if (this.enabledForm == false) {
      this.enabledForm = !this.enabledForm;
      this.userForm.get('first_name').disable();
      this.userForm.get('last_name').disable();
      this.userForm.get('identificacion').disable();
      this.userForm.get('whatsapp').disable();
      this.userForm.get('email').disable();
      this.userForm.get('Shipping_Address').disable();
    } else {
      this.enabledForm = !this.enabledForm;

      if (
        this.userForm.get('first_name').value == "" ||
        this.userForm.get('last_name').value == ""
      ) {
        this.userForm.get('first_name').enable();
        this.userForm.get('last_name').enable();
        // this.userForm.get('identificacion').enable();
        this.userForm.get('whatsapp').enable();
        this.userForm.get('Shipping_Address').enable();
        this.userForm.get('email').disable();
      } else {
        this.userForm.get('whatsapp').enable();
        this.userForm.get('Shipping_Address').enable();
      }

      return this.enviando = false;
    }
  }

  onSubmitUser() {
    let userData = [];
    let currentUpdate = {};
    this.enviando = true;
    // console.log(this.userForm.get('Shipping_Address').value !== '');
    if (this.userForm.get('Shipping_Address').value == '') {
      this._snackBar.open('Por favor agregar una dirección para continuar');
      this.enviando = false;
    } else {

      let whatsapp = this.userForm.get('whatsapp').value;
      let address = this.userForm.get('Shipping_Address').value;
      let userDoc = this.userForm.get('identificacion').value;

      userData.push(
        {
          'usuario': parseInt(this.userName[0].idUser),
          'identification': userDoc,
          'whatsapp': whatsapp,
          'Shipping_Address': address
        }
      );
      currentUpdate = Object.assign
        (
          {
            'username': this.userName[0].user,
            'first_name': this.userForm.get('first_name').value,
            'last_name': this.userForm.get('last_name').value
          }
        );
      let userDataUpdate = userData;
      console.log(userDataUpdate)
      this.disabledEnabled();
      console.log(currentUpdate)
      this.userService.updateCurrentUser(currentUpdate).subscribe(udateUser => {

        this.userService.getAll(this.userName[0].idUser).subscribe(data => {


          if (data[0] !== undefined) {
            this.userService.userUpdate(this.userName[0].idUser, userDataUpdate[0]).subscribe(data => {
              console.log(data);
              this.userForm.get('Shipping_Address').setValue(data['Shipping_Address'])
              // this.disabledEnabled();
              this.userService.getAll(this.userName[0].idUser).subscribe(data => {
                console.log(data)
                this.getUserData = data;
              })
              this._snackBar.open('Sus datos se actualizaron con exito', 'ok', {
                duration: 4000,
              });
              return this.enviando = false;
            }, err => {
              // this.disabledEnabled();
              this._snackBar.open('Ocurrió un error, intente más tarde', 'ok', {
                duration: 4000,
              });
              return this.enviando = false;
            })
          } else {


            this.userService.userCreateData(userDataUpdate).subscribe(data => {
              // this.disabledEnabled();
              this._snackBar.open('Sus datos se actualizaron con exito', 'ok', {
                duration: 4000,
              });
              return this.enviando = false;
            }, err => {
              // this.disabledEnabled();
              this._snackBar.open('Ocurrió un error, intente más tarde', 'ok', {
                duration: 4000,
              });
              return this.enviando = false;
            })
          }
        }, err => {
          this._snackBar.open('Ocurrió un error, intente más tarde', 'ok', {
            duration: 4000,
          });
          return this.enviando = false;
        })

      });
    }
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }

}
