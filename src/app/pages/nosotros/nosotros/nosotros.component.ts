import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.scss']
})
export class NosotrosComponent implements OnInit {


  constructor() { }


  ngOnInit() {
    AOS.init();
  }

}
