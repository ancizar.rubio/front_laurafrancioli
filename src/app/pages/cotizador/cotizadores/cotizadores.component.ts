import { StorageServiceService } from './../../../services/storage-service.service';
import { ProductService } from './../../../services/product.service';
import { OrderModalComponent } from './../../components/order-modal/order-modal.component';
import { OrderService } from './../../../services/order.service';
import { RegisterService } from './../../../services/register.service';
import { ModalterminoComponent } from './../../modaltermino/modaltermino.component';
import { UserService } from './../../../authService/user.service';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../../authService/authentication.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CotizadorService } from './../../../services/cotizador.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogPoliticsComponent } from './../../components/dialog-politics/dialog-politics.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { environment } from 'src/app/authService/environment.prod';
import { NguCarouselModule, NguCarouselConfig } from '@ngu/carousel';
import { ModalDireccionesComponent } from '../../components/modal-direcciones/modal-direcciones.component';
import * as jwt_decode from "jwt-decode";
import { DateAdapter, MatStepper } from '@angular/material';

export interface AllProducts {
  id?: number;
  codigo?: string;
  producto?: string;
  unidadMinima?: number;
  precio?: number;
  totalProducto?: number;
}


@Component({
  selector: 'app-cotizadores',
  templateUrl: './cotizadores.component.html',
  styleUrls: ['./cotizadores.component.scss']
})
export class CotizadoresComponent implements OnInit {

  cotizadorForm: FormGroup;
  registroForm: FormGroup;
  direccionFormPersonal: FormGroup;
  direccionFormLocal: FormGroup;
  paymentForm: FormGroup;
  submited = true;
  que_quieres = [];
  paraQue = [];
  dimension = []
  paraquelaobra = false;
  env = environment.apiUrl;
  showDescript = false;
  loginForm: FormGroup;
  loginInvalid: boolean;
  enviando = false;
  formSubmitAttempt: boolean;
  hideReg = true;
  hideLog = true;
  hideConf = true;
  islogged = false;
  tipoPersona = ['Natural', 'Juridica'];
  orientaciones = ['Horizontal', 'Vertical'];
  isLogReg = true;
  dataUserCot = [];
  imagesExist = false;
  // direccionesLocales = ['Entrega en fabrica o local de laura francioli, TURBACO', 'Entrega en fabrica o local de laura francioli, CARTAGENA'];
  direccionesLocales = [];
  // direccionesPersonales = ['carrera 77 b bis 78-04 Bogota', 'calle falsa 123'];
  direccionesPersonales = [];
  FormasDePago = ['Anticipo 50% del valor total en la cual se incluye la aprobación del diseño', 'Pago 100% del valor de la obra de arte el cual incluye el producto terminado entregado en el lugar indicado']
  fechasEntrega = ['Estándar', 'Especial'];
  usuarioPk = [];
  AddrsRegister = [];
  fieldsCompleted = true;
  @ViewChild('imagenUp1', { static: false }) imagenUp1: ElementRef;
  @ViewChild('imagenUp2', { static: false }) imagenUp2: ElementRef
  @ViewChild('imagenUp3', { static: false }) imagenUp3: ElementRef
  @ViewChild('stepper', { static: false }) stepper;
  orderNo = [{ 'invoiceNo': 0 }];
  private dialogRef: MatDialogRef<OrderModalComponent>
  transactions: AllProducts[];
  timeOut = 3000;
  totalPay = [];
  messageDialog = 'Estamos generando su orden';
  isValidCotizacion = false;
  @ViewChild('formWomp', { static: false }) formWomp: ElementRef;
  minDate = new Date();
  // maxDate = new Date();
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }

  imgags = [
    // this.env + '/media/ImagenesProducto/bod1.jpg',
    // this.env + '/media/ImagenesProducto/1561.jpg',
    // this.env + '/media/ImagenesProducto/ultima_cena.png',
    // this.env + '/media/ImagenesProducto/bod1.jpg',
    // this.env + '/media/ImagenesProducto/1561.jpg',
    // this.env + '/media/ImagenesProducto/ultima_cena.png',
  ];
  public carouselTileItems: Array<any> = [];
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
    slide: 1,
    speed: 250,
    point: {
      visible: true
    },
    load: 1,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)'
  };

  constructor(public dialog: MatDialog,
    public formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private cotizador: CotizadorService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private rservice: RegisterService,
    private orderShop: OrderService,
    private allProduct: ProductService,
    private globalSrv: StorageServiceService,
    private orderMake: OrderService,
    private dateAdapter: DateAdapter<any>) {
      this.dateAdapter.setLocale('co');
    }

  ngOnInit() {

    this.authenticationService.isLoggedIn.subscribe((value) => {
      this.islogged = value;
      if (this.islogged == true) {
        this.userService.getCurrentUser().subscribe(usuario => {
          this.usuarioPk = usuario;
          this.cotizador.getAllDirecusuario(usuario['pk']).subscribe(direcciones => {
            let addres = []
            if (direcciones != (null || undefined)) {
              addres.push(direcciones)
              this.direccionesPersonales = addres[0]
            }

          })
        })
      }
    })

    this.cotizadorForm = this.formBuilder.group({
      quequiere: ['', Validators.required],
      paraqueloquiere: [''],
      otroParaqueloquiere: [''],
      referencia: [1, Validators.required],
      dimensions: ['', Validators.required],
      orientacion: ['', Validators.required],
      imagen1: [''],
      imagen2: [''],
      imagen3: [''],
      // refobradearte: ['', Validators.required],
    });

    this.direccionFormLocal = this.formBuilder.group({
      direccion: ['', Validators.required]
    })

    this.direccionFormPersonal = this.formBuilder.group({
      direccion: ['', Validators.required]
    })

    // this.cotizador.getAllDirecusuario(['pk']).subscribe(direcciones => {
    //   this.direccionFormPersonal.push(direcciones)
    // })

    this.cotizador.getAllQue().subscribe(quequieres => {
      this.que_quieres.push(quequieres)
      console.log(quequieres)
    })

    this.cotizador.getAllParaque().subscribe(paraque => {
      this.paraQue.push(paraque)
      console.log(paraque)
    })

    this.cotizador.getAllDimensiones().subscribe(dim => {
      this.dimension.push(dim)
      console.log(dim)
    });

    this.cotizador.getAllDirecLocal().subscribe(dirlocal => {
      console.log(dirlocal)
      let localaddres = []
      localaddres.push(dirlocal)
      this.direccionesLocales = localaddres[0]
    })

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });


    this.registroForm = this.formBuilder.group({
      tipoUsuario: ['', [Validators.required]],
      documentNo: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.minLength(8), Validators.required]],
      confirm: ['', [Validators.minLength(8), Validators.required]],
    }, {
      validator: MustMatch('password', 'confirm')
    });

    this.paymentForm = this.formBuilder.group({
      cantidadaCotizar: [1, [Validators.required, Validators.min(1)]],
      formaDePago: ['', [Validators.required]],
      fechaEntrega: ['', [Validators.required]],
      diaEntrega: [{ value: '', disabled: true }, Validators.required],
      politicas: ['', [Validators.required]]
    })

    var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    console.log(utc);
    let date = utc.split('/');
    console.log(date, parseInt(date[1]))

    this.minDate = new Date(parseInt(date[0]), 7, parseInt(date[2]) + 3);

    this.cotizador.getAllRefs().subscribe(refs => {
      console.log(refs)
      let getRefs = [];
      getRefs.push(refs);
      this.imgags = getRefs[0].reverse()
      this.carouselTileItems = this.imgags;
    })

  }
  getCity = [];
  getDataCotizacion() {
    this.authenticationService.isLoggedIn.subscribe((value) => {
      this.islogged = value;
      if (this.islogged == true) {
        this.userService.getCurrentUser().subscribe(getCurrentUsr => {
          console.log(getCurrentUsr);
          this.userService.getAll(getCurrentUsr['pk']).subscribe(dataUser => {
            console.log(dataUser);
            this.getCity.push(dataUser[0]['ciudad'] !== null ? dataUser[0]['ciudad'] : null)
            let selectedDpto = [];
            this.userService.getDepartamentos().subscribe(allDptos => {
              selectedDpto.push(allDptos);

              let dptoSelected = selectedDpto[0].filter(x => x.id == dataUser[0]['departamento']);
              console.log(dptoSelected.length)

              let getDireccionLocal = this.direccionesLocales[this.direccionFormLocal.value.direccion];
              let getDireccionPersonal = this.direccionesPersonales[this.direccionFormPersonal.value.direccion];
              // let getDireccionPersonal = this.direccionesPersonales[this.direccionForm.value.direccion+'p']
              let currentAddress = getDireccionLocal !== undefined ? getDireccionLocal : getDireccionPersonal;
              console.log(currentAddress);
              this.dataUserCot = [{
                'Nombre': getCurrentUsr['first_name'],
                'Apellidos': getCurrentUsr['last_name'],
                'usuario': getCurrentUsr['username'],
                'identificacion': dataUser[0].identification,
                'email': getCurrentUsr['email'],
                'telefono': dataUser[0].whatsapp,
                'direccion': currentAddress,
                'departamento': dptoSelected.length !== 0 ? dptoSelected[0].departamento : undefined,
                'ciudad': this.getCity[0],
                'EmocionDespertarArte': this.cotizadorForm.controls.quequiere.value,
                'paraqueloquiere': this.cotizadorForm.controls.paraqueloquiere.value,
                'Descripción': this.cotizadorForm.controls.otroParaqueloquiere.value !== undefined ? this.cotizadorForm.controls.otroParaqueloquiere.value : '',
                'referencia': this.cotizadorForm.controls.referencia.value,
                'dimension': this.cotizadorForm.controls.dimensions.value,
                'orientacion': this.cotizadorForm.controls.orientacion.value
              }]

              // quequiere: ['', Validators.required],
              //       paraqueloquiere: [''],
              //       otroParaqueloquiere: [''],
              //       referencia: [1, Validators.required],
              //       dimensions: ['', Validators.required],
              //       orientacion: ['', Validators.required],
              //       imagen1: ['', Validators.required],
              //       imagen2: ['', Validators.required],
              //       imagen3: ['', Validators.required],
              console.log(this.dataUserCot);
              console.log(this.dataUserCot[0].Nombre.length, this.dataUserCot[0].Apellidos.length, this.dataUserCot[0].Apellidos == undefined, this.dataUserCot[0].Apellidos == null, this.dataUserCot[0].ciudad)

            })
          })
        })
      }
    });
  }

  addresLocal = false;
  addresPersonal = false;

  selectedOption(x) {
    if (x == 'f') {
      this.addresPersonal = true
      this.addresLocal = false
      this.maintainPrice = this.precioActual;
      console.log(this.maintainPrice)
    } else {
      this.addresLocal = true
      this.addresPersonal = false
    }
  }

  addressAsign(stepp) {
    stepp.next()
  }

  async openWompi() {
    let WidgetCheckout = await document.querySelectorAll('#wompiCheckout')
    console.log(WidgetCheckout);
    let getDataW = this.cotizador.getUrlWompi().subscribe(data => {
      console.log(data)

    })
    // var checkout = new WidgetCheckout({
    //   currency: 'COP',
    //   amountInCents: 2490000,
    //   reference: 'AD002901221',
    //   publicKey: 'pub_fENJ3hdTJxdzs3hd35PxDBSMB4f85VrgiY3b6s1',
    //   redirectUrl: './', // Opcional
    //   taxInCents: { // Opcional
    //     vat: 1900,
    //     consumption: 800
    //   }
    // });
    // checkout.open(function (result) {
    //   var transaction = result.transaction
    //   console.log('Transaction ID: ', transaction.id)
    //   console.log('Transaction object: ', transaction)
    // })
  }


  dialogPolitics() {
    const dialogRef = this.dialog.open(DialogPoliticsComponent, { disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        console.log('acepto las politicas')
      } else {
        console.log('no acepto');
        this._snackBar.open('Debe aceptar las politicas para iniciar la descarga', 'OK', {
          duration: 4000,
        });
      }
    });
  }



  showDescription(btn, item) {

    let card = item.parentNode.parentNode;

    if (item.style.display == 'none') {
      btn.disabled = true;
      item.style.display = 'block'
      card.classList.add('open-back');
      btn.disabled = false;
    } else {
      btn.disabled = true;
      item.classList.add('fadeOutDown');
      card.classList.remove('open-back');
      setTimeout(() => {
        item.classList.remove('fadeOutDown');
        item.style.display = 'none';
        btn.disabled = false;
      }, 600);

    }

  }

  onValueChanges(step: MatStepper) {
    console.log(this.isValidCotizacion);
    console.log(this.cotizadorForm.controls.imagen1.value !== '', this.cotizadorForm.controls.imagen2.value !== '', this.cotizadorForm.controls.imagen3.value !== '')
    if (this.cotizadorForm.controls.imagen1.value !== '' || this.cotizadorForm.controls.imagen2.value !== '' || this.cotizadorForm.controls.imagen3.value !== '') {
      this.isValidCotizacion = true;
      setTimeout(() => {           // or do some API calls/ Async events
        step.next();
      }, 1);
    } else {
      this._snackBar.open('Seleccione al menos una imágen', 'OK', {
        duration: 4000,
      });
      this.isValidCotizacion = false
    }

  }

  onSubmitlogin() {
    this.enviando = true;
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.loginForm.valid) {
      try {
        const email = this.loginForm.get('email').value;
        localStorage.setItem('email', JSON.stringify(email));
        const password = this.loginForm.get('password').value;
        this.authenticationService.login(email, password).subscribe(res => {
          this._snackBar.open('Bienvenido', 'ok', {
            duration: 2000,
          });
          this.userService.getCurrentUser().subscribe(user => {
            console.log(user)
            this.usuarioPk = user;
            this.cotizador.getAllDirecusuario(user['pk']).subscribe(direcciones => {
              let addres = []
              addres.push(direcciones)
              this.direccionesPersonales = addres[0]
            })
          })
          this.enviando = false;
          this.isLogReg = false;
        }, err => {
          this._snackBar.open('Usuario o contraseña no valido', 'ok', {
            duration: 2000,
          });
          this.enviando = false;
          this.isLogReg = true;
        });
      } catch (err) {
        this.loginInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
      this.enviando = false;
    }
  }

  precioActual = 0

  getPrice(precio) {
    this.precioActual = precio
  }

  submitted = false;

  userRegister() {
    const dialogRef = this.dialog.open(ModalterminoComponent, {
      panelClass: 'modal-politics'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.submitted = true;
        let valueRegister = { 'email': this.registroForm.value.email, 'password1': this.registroForm.value.password, 'password2': this.registroForm.value.confirm }
        console.log(valueRegister)
        this.rservice.register(valueRegister).subscribe(data => {
          this.authenticationService.login(valueRegister.email, valueRegister.password1).subscribe(res => {
            console.log(res)

            this.userService.getCurrentUser().subscribe(userCurrent => {
              console.log(userCurrent)
              this.usuarioPk = userCurrent;
              let userData = [];
              this._snackBar.open('usuario creado con éxito', 'ok', {
                duration: 4000,
              });
              this.cotizador.getAllDirecusuario(userCurrent['pk']).subscribe(direcciones => {
                let addres = []
                if (direcciones != (null || undefined)) {
                  addres.push(direcciones)
                  this.direccionesPersonales = addres[0]
                }

              })
              userData.push(
                {
                  'usuario': parseInt(userCurrent['pk']),
                  'persona': this.registroForm.value.tipoUsuario,
                  'identification': this.registroForm.value.documentNo,
                }
              );
              this.userService.userCreateData(userData).subscribe(updateUser => {
                this.submitted = false;
                this.registroForm.updateValueAndValidity({
                  onlySelf: true
                });
                this._snackBar.open('Bienvenido', 'ok', {
                  duration: 2000,
                });
              }, err => {
                this.submitted = false;
                this._snackBar.open('No fue posible crear los datos Tipo persona y No de documento', 'ok', {
                  duration: 2000,
                });
              })
            })


          }, err => {
            this.submitted = false;
            this._snackBar.open('Usuario o contraseña no valido', 'ok', {
              duration: 2000,
            });
          });


          //this.router.navigate(['/login']);
        }, err => {
          this.submitted = false;
          this._snackBar.open('No se logro registrar el usuario, compruebe los datos suministrados', 'ok', {
            duration: 6000,
          });
        });
      } else {
        this._snackBar.open('para continuar, acepta nuestras politicas', 'ok', {
          duration: 4000,
        });
      }
    })
  }

  valueByDpto = 0;

  openModalAddrs(): void {
    console.log(this.dimension)
    let getDimensionSelected = this.dimension[0].filter(x => x.id == this.cotizadorForm.controls.dimensions.value)
    console.log(getDimensionSelected);
    // this.cotizadorForm.controls.dimensions.value, this.dimension
    const dialogRef = this.dialog.open(ModalDireccionesComponent, {
      width: '300px',
      data: [this.usuarioPk, getDimensionSelected]
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result)
      if (result !== undefined) {
        this.AddrsRegister = result;
        console.log(this.usuarioPk)
        this.userService.getAll(this.usuarioPk['pk']).subscribe(datosUser => {
          console.log(datosUser)
          let setDireccion = [
            {
              'usuario': this.usuarioPk['pk'],
              'direccion': this.AddrsRegister[0].direccion
            }
          ];
          this.userService.setAddrs(setDireccion).subscribe(resDireccion => {
            let updateDataUser =
            {
              "id": datosUser[0]['id'],
              "usuario": this.usuarioPk['pk'],
              "persona": datosUser[0]['persona'],
              "departamento": this.AddrsRegister[0].departamento,
              "ciudad": this.AddrsRegister[0].ciudad,
              "whatsapp": null,
              "identification": datosUser[0]['identification'],
              "Shipping_Address": resDireccion[0].id
            }
            console.log(updateDataUser);
            this.userService.userUpdate(datosUser[0]['id'], updateDataUser).subscribe(dataUser => {
              console.log(dataUser);

              this._snackBar.open('Dirección agregada con éxito', 'ok', {
                duration: 4000,
              });

              this.cotizador.getAllDirecusuario(this.usuarioPk['pk']).subscribe(direcciones => {
                let addres = []
                if (direcciones != (null || undefined)) {
                  addres.push(direcciones)
                  this.direccionesPersonales = addres[0]
                }

              })
            }, err => {
              console.log(err);
              this._snackBar.open('Error al agregar la dirección', 'ok', {
                duration: 4000,
              });
            })
          }, err => {
            console.log(err);
            this._snackBar.open('No se pudo guardar la dirección', 'ok', {
              duration: 4000,
            });
          })
        })
      } else {
        this._snackBar.open('Dirección cancelada', 'ok', {
          duration: 4000,
        });
      }
    });
  }

  maintainPrice = 0;

  optionDpto(idUser) {
    //this.valueByDpto
    this.userService.getCurrentUser().subscribe(getCurrentUsr => {
      console.log(getCurrentUsr);
      this.userService.getAll(getCurrentUsr['pk']).subscribe(dataUser => {
        console.log(dataUser)
        let dptoAll = [];
        this.userService.getDepartamentos().subscribe(dptos => {

          dptoAll.push(dptos);
          console.log(dptoAll);
          let getDpto = dptoAll[0].filter(x => x.id == dataUser[0]['departamento']);
          let getDimension = this.dimension[0].filter(x => x.id == this.cotizadorForm.controls.dimensions.value);
          console.log(getDimension)
          let getPriceDimension = 0;
          getPriceDimension = getDimension[0].precio;
          console.log(getPriceDimension)
          let getPriceByMedida = dptoAll[0].filter(x => x.dimesion == getDimension[0].dimension);
          console.log(getPriceByMedida);
          console.log(getDpto)
          let nameDptoPrice = getPriceByMedida.filter(x => x.departamento == getDpto[0].departamento);
          console.log(nameDptoPrice)
          this.valueByDpto = nameDptoPrice[0].precio;
          console.log(this.valueByDpto)
          console.log(this.precioActual)
          let newPrice = 0;
          newPrice = parseInt(getPriceDimension.toString()) + parseInt(this.valueByDpto.toString());
          console.log(newPrice)
          this.precioActual = newPrice
          this.maintainPrice = this.precioActual;
        })
      })
    })
  }

  valorPendiente = 0;

  calculePorcent(porcent) {
    console.log(porcent)
    let getPercent = porcent.match(/\d+/g);
    console.log(getPercent)
    let newPrice = 0;
    console.log(this.maintainPrice)
    if (getPercent[0] == '50') {
      newPrice = Math.floor(this.precioActual * parseInt(getPercent) / 100);
      this.precioActual = newPrice
      this.valorPendiente = newPrice
      this.paymentForm.controls.fechaEntrega.reset()
    } else {
      console.log(this.maintainPrice)
      this.precioActual = this.maintainPrice;
      console.log(this.precioActual)
      this.valorPendiente = 0;
      this.paymentForm.controls.fechaEntrega.reset()
    }
  }

  deliveryDate(date) {
    let newPrice = 0;
    console.log(date,this.paymentForm.controls.formaDePago.value)
    if (date == 'Especial' && this.paymentForm.controls.formaDePago.value.includes(50)) {
      newPrice = Math.floor((this.precioActual * 30) / 100);
      this.precioActual = this.precioActual + newPrice;
      this.valorPendiente = this.valorPendiente + newPrice;
    } else if (date == 'Estándar' && this.paymentForm.controls.formaDePago.value.includes(50)) {
      let newPrice = Math.floor(this.maintainPrice * 50 / 100);
      this.precioActual = this.maintainPrice - newPrice;
      this.valorPendiente = this.maintainPrice - newPrice
    } else if (date == 'Especial' && this.paymentForm.controls.formaDePago.value.includes(100)) {
      newPrice = Math.floor((this.maintainPrice * 30) / 100);
      console.log(this.maintainPrice, newPrice);
      let mainPrice = [];
      mainPrice.push(this.maintainPrice);
      console.log(mainPrice)
      this.precioActual = parseInt(mainPrice[0]) + newPrice;
      console.log(this.precioActual)
      this.valorPendiente = 0
    } else if (date == 'Estándar' && this.paymentForm.controls.formaDePago.value.includes(100)) {
      this.precioActual = this.maintainPrice;
      this.valorPendiente = 0
    }
  }

  selectionChange(ev) {
    console.log(ev)
    if (ev.selectedIndex == 0) {
      this.direccionFormLocal.reset();
      this.direccionFormPersonal.reset()
    }
  }

  saveInpt(fieldValue) {
    console.log(fieldValue);
    let currentUpdate = {};
    this.userService.getCurrentUser().subscribe(getCurrentUsr => {
      console.log(getCurrentUsr);
      if (fieldValue.nombres !== undefined) {
        currentUpdate = Object.assign
          (
            {
              'username': getCurrentUsr['username'],
              'first_name': fieldValue.nombres,
            }
          );
        this.userService.updateCurrentUser(currentUpdate).subscribe(userUpdate => {
          this.getDataCotizacion()
          this._snackBar.open('Nombres guardados con éxito', 'ok', {
            duration: 4000,
          });
          setTimeout(() => {
            // this.validarDatosBasicosNAT()
          }, 4000);
        }, err => {
          this._snackBar.open('Error al guardar nombres', 'ok', {
            duration: 4000,
          });
        })

      } else if (fieldValue.apellidos !== undefined) {
        currentUpdate = Object.assign
          (
            {
              'username': getCurrentUsr['username'],
              'last_name': fieldValue.apellidos,
            }
          );
        this.userService.updateCurrentUser(currentUpdate).subscribe(userUpdate => {
          this.getDataCotizacion()
          this._snackBar.open('Apellidos guardados con éxito', 'ok', {
            duration: 4000,
          });
          setTimeout(() => {
            // this.validarDatosBasicosNAT()
          }, 4000);
        }, err => {
          this._snackBar.open('Error al guardar apellidos', 'ok', {
            duration: 4000,
          });
        })
      } else if (fieldValue.telefono !== undefined) {
        console.log('telefono')
        this.userService.getAll(this.usuarioPk['pk']).subscribe(datosUser => {
          console.log(datosUser)
          let updateDataUser =
          {
            "id": datosUser[0]['id'],
            "usuario": this.usuarioPk['pk'],
            "whatsapp": fieldValue.telefono
          }
          this.userService.userUpdate(datosUser[0]['id'], updateDataUser).subscribe(updateDataUser => {
            this.getDataCotizacion()
            this._snackBar.open('Teléfono guardado con éxito', 'ok', {
              duration: 4000,
            });
            setTimeout(() => {
              // this.validarDatosBasicosNAT()
            }, 4000);
          }, err => {
            this._snackBar.open('Error al guardar el teléfono', 'ok', {
              duration: 4000,
            });
          })
        })
      }
    })
    // let userUpdate =
    // this.userService.updateCurrentUser()


  }

  validarDatosBasicosNAT() {
    if (this.dataUserCot[0].Nombre.length !== 0 && this.dataUserCot[0].Apellidos.length !== 0 && this.dataUserCot[0].Telefono.length !== 0) {
      this.fieldsCompleted = false
    } else {
      this.fieldsCompleted = true;
      this._snackBar.open('Complete los campos de información (Nombres, Apellidos, Teléfono)', 'OK', {
        duration: 7000,
      });
    }
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  // fileData: File = null;

  getImagen1: File = null;
  getImagen2: File = null;
  getImagen3: File = null;
  getAllImages = []
  // previewUrl: any = null;
  // formData = new FormData();
  fileProgress(fileInput: any) {
    // this.getImagen1 = <File>fileInput.target.files[0];
    if (this.getImagen1 == undefined) {
      this.getImagen1 = <File>fileInput.target.files[0];
    } else if (this.getImagen2 == undefined) {
      this.getImagen2 = <File>fileInput.target.files[0];
    } else if (this.getImagen3 == undefined) {
      this.getImagen3 = <File>fileInput.target.files[0];
    }

    console.log(this.getImagen1, this.getImagen2, this.getImagen3)
    this.getAllImages.push({ 'nombre': 'imagen', 'img': this.getImagen1 }, { 'nombre': 'imagen2', 'img': this.getImagen2 }, { 'nombre': 'imagen3', 'img': this.getImagen3 })
  }

  loadMakeCotizacion = false;
  idCotizacion = [];
  getTotal = 0;

  generarCotizacion() {
    this.loadMakeCotizacion = true;
    let makeCotizador = {};
    let order = [];

    console.log(this.dataUserCot[0]);

    console.log(this.cotizadorForm.controls.imagen1.value, this.cotizadorForm.controls.imagen2.value, this.cotizadorForm.controls.imagen3.value)

    let getImages = [this.cotizadorForm.controls.imagen1.value, this.cotizadorForm.controls.imagen2.value, this.cotizadorForm.controls.imagen3.value]

    if (this.dataUserCot[0].Nombre == '') {
      console.log('faltan nombres');
      this._snackBar.open('Agregue nombre(s) para continuar', 'OK', {
        duration: 4000,
      });
      this.loadMakeCotizacion = false;
    } else if (this.dataUserCot[0].Apellidos == '') {
      console.log('faltan apellidos');
      this._snackBar.open('Agregue Apellido(s) para continuar', 'OK', {
        duration: 4000,
      });
      this.loadMakeCotizacion = false;
    } else if (this.dataUserCot[0].Telefono == '') {
      console.log('falta teléfono')
      this._snackBar.open('Agregue Teléfono(s) para continuar', 'OK', {
        duration: 4000,
      });
      this.loadMakeCotizacion = false;
    } else {
      this.userService.getCurrentUser().subscribe(getCurrentUsr => {
        console.log(getCurrentUsr);
        let valuePay = (this.paymentForm.controls.formaDePago.value).match(/\d+/g);
        let fieldValutTotal = document.getElementById('element-value').innerHTML;
        let getValueTotal = fieldValutTotal.slice(2);
        console.log(getValueTotal.replace(/\D/g, ""))
        let total = parseInt(getValueTotal.replace(/\D/g, ""));
        console.log(total)
        this.getTotal = total;
        console.log(this.getTotal)
        makeCotizador = [{
          "usuario": getCurrentUsr['pk'],
          "que_quieres": this.dataUserCot[0].EmocionDespertarArte,
          "para_que": this.dataUserCot[0].paraqueloquiere,
          "otro": "",
          "referencia": this.dataUserCot[0].referencia,
          "dimension": this.dataUserCot[0].dimension,
          "orientacion": this.dataUserCot[0].orientacion == 0 ? 'H' : 'V',
          "pagos": valuePay[0],
          "forma_entrega": this.paymentForm.controls.fechaEntrega.value,
          "rango_entrega": (this.paymentForm.controls.diaEntrega.value).toLocaleDateString().slice(0,10),
          "departamento": this.dataUserCot[0].departamento,
          "ciudad": this.getCity[0],
          "direccion": this.dataUserCot[0].direccion.direccion,
          "cantidad": this.paymentForm.controls.cantidadaCotizar.value,
          "estado_pago": "pendiente",
          "estado_cotizacion": "En Gestión",
          "saldo_pendiente": this.valorPendiente > 0 ? this.valorPendiente : 0,
          "precio": this.getTotal,
          "estado": null
        }]
        console.log('listo para guardar');
        console.log(makeCotizador)

        this.cotizador.setCotizacion(makeCotizador).subscribe(resCotizacion => {
          console.log(resCotizacion);
          let setImage = [];
          this.idCotizacion.push(resCotizacion[0].id)
          console.log(this.idCotizacion)
          const formData = new FormData()

          formData.append('cotizador', JSON.stringify(resCotizacion[0].id))

          this.getAllImages.forEach(image => {
            if (image.img !== null) {
              formData.append(image.nombre, image.img)
            }
          })

          this.cotizador.setImagesByCotizacion(formData).subscribe(imagesByCotRes => {
            console.log('imagenes cargadas', imagesByCotRes)

            this._snackBar.open('Cotización creada con éxito', 'OK', {
              duration: 4000,
            });
            order.push({ 'usuario': getCurrentUsr['pk'], 'estado': 'pendiente', 'precio_total': this.getTotal });
            this.orderMake.makeOrderShop(order).subscribe(resOrder => {
              this.messageDialog = 'Un momento, ya casí terminamos'
              this.dialogRef = this.dialog.open(OrderModalComponent, {
                height: '100px',
                disableClose: true,
                data: { messageDialog: this.messageDialog }
              });
              this.orderNo = [];
              console.log(this.orderNo)
              this.orderNo.push({ 'invoiceNo': resOrder[0].invoice_no });
              console.log(this.orderNo.length > 0, this.orderNo)
              console.log(this.orderNo)

              if (this.orderNo.length > 0) {
                this.dialogRef.close();
                this.messageDialog = 'Será redirigido a la opción de pago.';
                this.dialogRef = this.dialog.open(OrderModalComponent, {
                  height: '100px',
                  disableClose: true,
                  data: { messageDialog: this.messageDialog }
                });

                // console.log(document.getElementById('formularioWompi'))
                this.loadMakeCotizacion = false;
                this.cotizadorForm.reset();
                this.direccionFormLocal.reset();
                this.paymentForm.reset();
                this.paymentForm.controls.cantidadaCotizar.setValue(1)
                this.stepper.reset()
                this.stepper.selectedIndex = 0;
                this.isValidCotizacion = false
                setTimeout(() => {
                  this.dialogRef.close();
                  this.formWomp.nativeElement.submit();
                }, 3000);
                setTimeout(() => {
                  this.precioActual = 0;
                }, 4000);

              }
            }, err => {
              console.log('No se logro conectar con la pasarela de pago', err);
            })


          }, err => {
            console.log('No se cargaron las imagenes', err);
            this.loadMakeCotizacion = false;
          })
        }, err => {
          this._snackBar.open('Error al crear la cotización', 'OK', {
            duration: 4000,
          });
          this.loadMakeCotizacion = false;
        })
      })

    }



    // this.cotizador.setCotizacion(this.dataUserCot[0]).subscribe(resCotizacion => {
    //   this.cotizador.setImagesByCotizacion(this.dataUserCot[0]).subscribe(imagesByCotRes => {

    //   })
    // })
  }

  // sumartotales(total, value) {
  //   // console.log(total, value)
  //   let newValue = parseInt(total) * value;
  //   // console.log(newValue)
  //   this.precioActual = newValue;
  //   //let newTotal = this.precioActual
  // }

}

function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}




