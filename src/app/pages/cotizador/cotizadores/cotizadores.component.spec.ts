import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadoresComponent } from './cotizadores.component';

describe('CotizadoresComponent', () => {
  let component: CotizadoresComponent;
  let fixture: ComponentFixture<CotizadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
