import { User } from './../models/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../authService/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class CotizadorService {

  constructor(private http: HttpClient) { }

  getAllQue() {
    return this.http.get(`${environment.apiUrl}/cotizador/opcionque/`);
  }

  getAllParaque() {
    return this.http.get(`${environment.apiUrl}/cotizador/opcionparaque/`);
  }

  getAllDimensiones() {
    return this.http.get(`${environment.apiUrl}/referencia/precioreferencia/`);
  }

  getAllCotizaciones(id) {
    return this.http.get(`${environment.apiUrl}/cotizador/cotizaciones/${id}`)
  }

  getCotizacionesByFilter(estado) {
    return this.http.get(`${environment.apiUrl}/cotizador/estadocot/?search=${estado}`)
  }

  getUrlWompi() {
    return this.http.get('https://checkout.wompi.co/widget.js');
  }

  getAllRefs() {
    return this.http.get(`${environment.apiUrl}/referencia/ref/`)
  }
  getAllDirecusuario(idUser) {
    return this.http.get<User[]>(`${environment.apiUrl}/direcusuario/retrieve/${idUser}`)
  }

  getAllDirecLocal() {
    return this.http.get(`${environment.apiUrl}/direcusuario/direclocal/`)
  }

  setCotizacion(cotizacion) {
    return this.http.post(`${environment.apiUrl}/cotizador/postcotizaciones/`, cotizacion)
  }

  updateCotizacion(idCotizacion, dataCotizacion) {
    return this.http.put(`${environment.apiUrl}/cotizador/actualizarestado/${idCotizacion}/`, dataCotizacion)
  }

  setImagesByCotizacion(images) {
    return this.http.post(`${environment.apiUrl}/imagencotizador/`, images)
  }

  getImagesByCotizacion(idCotizacion) {
    return this.http.get(`${environment.apiUrl}/cotizador/img/${idCotizacion}`)
  }

}
