import { TestBed } from '@angular/core/testing';

import { CotizadorService } from './cotizador.service';

describe('CotizadorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CotizadorService = TestBed.get(CotizadorService);
    expect(service).toBeTruthy();
  });
});
