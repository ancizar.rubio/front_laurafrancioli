export const MY_DATE_FORMATS = {
    analizar: {
      dateInput: 'AAAA-MM-DD',
    },
    mostrar: {
      dateInput: 'AAAA-MM-DD',
      monthYearLabel: 'MMM AAAA',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM AAAA'
    },
};
