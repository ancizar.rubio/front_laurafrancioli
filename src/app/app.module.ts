// import { MatAutocompleteModule } from '@angular/material/autocomplete';
// import { MatSnackBarModule } from '@angular/material/snack-bar';
//import { fakeBackendProvider } from './fake-backend';
import { MatIconModule } from '@angular/material/icon';
import { DemoMaterialModule } from './pages/material-module';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SlidercarruselComponent } from './pages/components/slidercarrusel/slidercarrusel.component';
import { NguCarouselModule } from '@ngu/carousel';
import { NuestrosclientesComponent } from './pages/components/clientes/nuestrosclientes/nuestrosclientes.component';
import { FooterComponent } from './pages/components/footer/footer/footer.component';
import { ProductoComponent } from './pages/productos/producto/producto.component';
import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './pages/maps/map/map.component';
import { ProductDetailComponent } from './pages/detail/product-detail/product-detail.component';
import { NgxGalleryModule, CustomHammerConfig } from 'ngx-gallery';
import { BasicAuthInterceptor, BASIC_AUTH_EXCLUDES } from './basic-auth-interceptor';
import { ErrorInterceptor } from './error-interceptor';
import { CartComponent } from './pages/shoppingCart/shoppingcart/cart.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './pages/registeruser/register/register.component';
import { LoginComponent } from './pages/login/login/login.component';
import { ProductminComponent } from './pages/components/productmin/productmin.component';
import { UserPanelComponent } from './pages/user-panel/user-panel.component';
import { NosotrosComponent } from './pages/nosotros/nosotros/nosotros.component';
import { EnviosDevComponent } from './pages/envios/envios-dev/envios-dev.component';
import { NuestrasMarcasComponent } from './pages/components/marca/nuestras-marcas/nuestras-marcas.component';
import { PoliticasDatosComponent } from './pages/politica/politicas-datos/politicas-datos.component';
import { BlogsComponent } from './pages/blog/blogs/blogs.component';
import { OrderModalComponent } from './pages/components/order-modal/order-modal.component';
import { OrdenesdecompraComponent } from './pages/ordenesdecompra/ordenesdecompra.component';
import { UserformComponent } from './pages/components/userform/userform.component';
import { SuccesPaymentComponent } from './pages/succes-payment/succes-payment.component';
import { ErrorPaymentComponent } from './pages/error-payment/error-payment.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { RestoreComponent } from './pages/restorepass/restore/restore.component';
import { NuevopassComponent } from './pages/nuevopass/nuevopass.component';
//import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
//import { statusInterceptor } from './status.interceptor';
import { HttpBackendClientService } from './services/http-backend-client.service';
import { DetailBlogComponent } from './pages/detail-blog/detail-blog.component';
import { ComoComprarComponent } from './pages/como-comprar/como-comprar.component';
import { PipeHtmlPipe } from './pipes/pipe-html.pipe';
import { TerminosComponent } from './pages/terminos/terminos.component';
import { ModalterminoComponent } from './pages/modaltermino/modaltermino.component';
import { CotizadoresComponent } from './pages/cotizador/cotizadores/cotizadores.component';
import { DialogPoliticsComponent } from './pages/components/dialog-politics/dialog-politics.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { GalleryModalComponent } from './pages/gallery-modal/gallery-modal.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { CotizacionesComponent } from './pages/cotizaciones/cotizaciones.component';
import { ModalDireccionesComponent } from './pages/components/modal-direcciones/modal-direcciones.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MY_DATE_FORMATS } from './MY_DATE_FORMATS';
import { CotizacionesAsignadasComponent } from './pages/cotizaciones-asignadas/cotizaciones-asignadas.component';
import { ModalEvidenciasComponent } from './pages/components/cotizaciones-asignadas/modal-evidencias/modal-evidencias.component';
import { ModalDetailUserCotizacionComponent } from './pages/components/modal-detail-user-cotizacion/modal-detail-user-cotizacion.component';
import { EmailSendToClientComponent } from './pages/components/email-send-to-client/email-send-to-client.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SlidercarruselComponent,
    NuestrosclientesComponent,
    FooterComponent,
    ProductoComponent,
    MapComponent,
    ProductDetailComponent,
    CartComponent,
    RegisterComponent,
    LoginComponent,
    ProductminComponent,
    UserPanelComponent,
    NosotrosComponent,
    EnviosDevComponent,
    NuestrasMarcasComponent,
    PoliticasDatosComponent,
    BlogsComponent,
    OrderModalComponent,
    OrdenesdecompraComponent,
    UserformComponent,
    SuccesPaymentComponent,
    ErrorPaymentComponent,
    RestoreComponent,
    NuevopassComponent,
    DetailBlogComponent,
    ComoComprarComponent,
    PipeHtmlPipe,
    TerminosComponent,
    ModalterminoComponent,
    CotizadoresComponent,
    DialogPoliticsComponent,
    GalleryComponent,
    GalleryModalComponent,
    CotizacionesComponent,
    ModalDireccionesComponent,
    CotizacionesAsignadasComponent,
    ModalEvidenciasComponent,
    ModalDetailUserCotizacionComponent,
    EmailSendToClientComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    // HttpClientJsonpModule,
    HttpClientModule,
    // MatIconModule,
    FlexLayoutModule,
    NguCarouselModule,
    NgxGalleryModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    MatGridListModule,
    // MatSnackBarModule,
    // MatAutocompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDFJ0cpwr1wHdktxm9lzKBAp7AcYdy8Yag'
    })
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig
    },
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    {
      provide: BASIC_AUTH_EXCLUDES,
      useValue: /^https:\/\/sandbox.wompi.co.v1.transactions/,
      multi: true
    },
    { provide: MAT_DATE_LOCALE, useValue: MY_DATE_FORMATS },
    //{ provide: HTTP_INTERCEPTORS, useClass: statusInterceptor, multi: true },
    HttpBackendClientService,
    // provider used to create fake backend
    //fakeBackendProvider
    /*{provide: APP_BASE_HREF, useValue: '/'},
    {provide: LocationStrategy, useClass: HashLocationStrategy}*/
  ],
  bootstrap: [AppComponent],
  exports: [OrderModalComponent, DialogPoliticsComponent, ModalEvidenciasComponent, ModalDetailUserCotizacionComponent, EmailSendToClientComponent],
  entryComponents: [OrderModalComponent, ModalterminoComponent, DialogPoliticsComponent, GalleryModalComponent, ModalDireccionesComponent, ModalEvidenciasComponent, ModalDetailUserCotizacionComponent, EmailSendToClientComponent],
})

export class AppModule { }
